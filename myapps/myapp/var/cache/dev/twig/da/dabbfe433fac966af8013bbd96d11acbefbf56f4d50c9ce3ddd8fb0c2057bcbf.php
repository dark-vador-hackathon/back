<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* profil.html.twig */
class __TwigTemplate_dd0dc36a4012af79d8ace279b4b0119c35ebc5215cc102cedaf4fabd0449edfb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "profil.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "profil.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "profil.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->loadTemplate("navbar.html.twig", "profil.html.twig", 3)->display($context);
        // line 4
        echo "
<div class=\"card\" style=\"width: 18rem;\">
  <img src=\"/img/profil.png\" class=\"card-img-top\" >
  <div class=\"card-body\">
    <h5 class=\"card-title\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 8, $this->source); })()), "email", [], "any", false, false, false, 8), "html", null, true);
        echo "</h5>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">An item</li>
    <li class=\"list-group-item\">A second item</li>
    <li class=\"list-group-item\">A third item</li>
  </ul>
  <div class=\"card-body\">
    <a href=\"#\" class=\"card-link\">Card link</a>
    <a href=\"#\" class=\"card-link\">Another link</a>
  </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "profil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  50 => 4,  48 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include 'base.html.twig' %}

{% include 'navbar.html.twig' %}

<div class=\"card\" style=\"width: 18rem;\">
  <img src=\"/img/profil.png\" class=\"card-img-top\" >
  <div class=\"card-body\">
    <h5 class=\"card-title\">{{ user.email }}</h5>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">An item</li>
    <li class=\"list-group-item\">A second item</li>
    <li class=\"list-group-item\">A third item</li>
  </ul>
  <div class=\"card-body\">
    <a href=\"#\" class=\"card-link\">Card link</a>
    <a href=\"#\" class=\"card-link\">Another link</a>
  </div>
</div>", "profil.html.twig", "/app/myapp/templates/profil.html.twig");
    }
}
