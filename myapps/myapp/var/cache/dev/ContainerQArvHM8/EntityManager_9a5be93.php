<?php

namespace ContainerQArvHM8;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder25a5b = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializercd554 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties93999 = [
        
    ];

    public function getConnection()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getConnection', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getMetadataFactory', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getExpressionBuilder', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'beginTransaction', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->beginTransaction();
    }

    public function getCache()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getCache', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getCache();
    }

    public function transactional($func)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'transactional', array('func' => $func), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'wrapInTransaction', array('func' => $func), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'commit', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->commit();
    }

    public function rollback()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'rollback', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getClassMetadata', array('className' => $className), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'createQuery', array('dql' => $dql), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'createNamedQuery', array('name' => $name), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'createQueryBuilder', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'flush', array('entity' => $entity), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'clear', array('entityName' => $entityName), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->clear($entityName);
    }

    public function close()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'close', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->close();
    }

    public function persist($entity)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'persist', array('entity' => $entity), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'remove', array('entity' => $entity), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'refresh', array('entity' => $entity), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'detach', array('entity' => $entity), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'merge', array('entity' => $entity), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getRepository', array('entityName' => $entityName), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'contains', array('entity' => $entity), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getEventManager', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getConfiguration', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'isOpen', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getUnitOfWork', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getProxyFactory', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'initializeObject', array('obj' => $obj), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'getFilters', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'isFiltersStateClean', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'hasFilters', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return $this->valueHolder25a5b->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializercd554 = $initializer;

        return $instance;
    }

    public function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config)
    {
        static $reflection;

        if (! $this->valueHolder25a5b) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder25a5b = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder25a5b->__construct($conn, $config);
    }

    public function & __get($name)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, '__get', ['name' => $name], $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        if (isset(self::$publicProperties93999[$name])) {
            return $this->valueHolder25a5b->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder25a5b;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder25a5b;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, '__set', array('name' => $name, 'value' => $value), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder25a5b;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder25a5b;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, '__isset', array('name' => $name), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder25a5b;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder25a5b;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, '__unset', array('name' => $name), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder25a5b;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder25a5b;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, '__clone', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        $this->valueHolder25a5b = clone $this->valueHolder25a5b;
    }

    public function __sleep()
    {
        $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, '__sleep', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;

        return array('valueHolder25a5b');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializercd554 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializercd554;
    }

    public function initializeProxy() : bool
    {
        return $this->initializercd554 && ($this->initializercd554->__invoke($valueHolder25a5b, $this, 'initializeProxy', array(), $this->initializercd554) || 1) && $this->valueHolder25a5b = $valueHolder25a5b;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder25a5b;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder25a5b;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
